import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tcg_companion/pages/userRegister.dart';
import 'drawerWithState.dart';

class UserRegisterFirst extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  static const String _title = 'Cadastro de Usuário Primeiro Passo';
  User currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(_title),
        backgroundColor: Colors.black,
      ),
      drawer: stateDrawer(),
      body: _body(context),
    );
  }

  Container _body(context) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextFormField(
              decoration: const InputDecoration(
                labelText: 'E-mail',
              ),
              controller: emailController,
              keyboardType: TextInputType.text,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Por Favor entre com o E-mail@';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: const InputDecoration(
                labelText: 'Senha',
              ),
              controller: passwordController,
              keyboardType: TextInputType.text,
              obscureText: true,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Por Favor entre com a sua senha';
                }
                return null;
              },
            ),
            buttonsRow(context)
          ],
        ),
      ),
    );
  }

  Future<void> _addUser(context) async {
    String email = emailController.text;
    String password = passwordController.text;

    try {
      UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      currentUser = userCredential.user;
    } on FirebaseAuthException catch (e) {
      print(e.code);
      if (e.code == 'email-already-in-use') {
        showAlertErrors(context, 'E-mail já está cadastrado');
      } else if (e.code == 'weak-password') {
        showAlertErrors(context, 'Senha fraca');
      } else if (e.code == 'invalid-email') {
        showAlertErrors(context, 'E-mail invalido');
      }
    }
  }

  showAlertDialog1(context) {

    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => UserRegister())
        );
      },
    );
    // configura o  AlertDialog
    AlertDialog alerta = AlertDialog(
      title: Text("Usuário Registrado com sucesso, favor para completar seu cadastro, colocar as informações pessoais!"),
      actions: [
        okButton,
      ],
    );
    // exibe o dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alerta;
      },
    );
  }

  showAlertErrors(context, String message) {

    Widget okButton = FlatButton(
      child: Text("Ok"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // configura o  AlertDialog
    AlertDialog alerta = AlertDialog(
      title: Text(message),
      actions: [
        okButton,
      ],
    );
    // exibe o dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alerta;
      },
    );
  }

 Row buttonsRow(context) {
    return Row(
      children: <Widget>[
        RaisedButton(
            child: new Text("Voltar"),
            textColor: Colors.white,
            color: Colors.redAccent,
            onPressed: () {
              // Navigate back to first screen when tapped!
              Navigator.pop(context);
            }
        ),
        RaisedButton(
          onPressed: () {
            if (_formKey.currentState.validate()) {
              _addUser(context);
              showAlertDialog1(context);
            }
          },
          child: Text('Avança'),
          textColor: Colors.white,
          color: Colors.redAccent,
        )
      ],
    );
  }
}