import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tcg_companion/pages/tournamentPage.dart';

import 'drawerWithState.dart';
import 'eventPage.dart';

class Tournaments extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Torneios'),
        backgroundColor: Colors.black,
      ),
      drawer: stateDrawer(),
      body: _body(context),
      backgroundColor: Colors.black,
    );
  }

  Column _body(context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: StreamBuilder<QuerySnapshot>(
            stream: FirebaseFirestore.instance.collection('tournaments').snapshots(),
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                case ConnectionState.waiting:
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                default:
                  List<DocumentSnapshot> tournaments = snapshot.data.docs;
                  return ListView.builder(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 050),
                    itemCount: tournaments.length,
                    itemBuilder: (context, index) {
                      return Card(
                          child: ListTile(
                            title: Text(tournaments[index].data()['name']),
                            subtitle: Text("Horário: ${tournaments[index].data()["time"]} - Valor de Inscrição: R\$${tournaments[index].data()["subs"]}"),
                            onTap: (){
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => new TournamentPage(tournament: tournaments[index]))
                              );
                            },
                          )
                      );
                    },
                  );
              }
            },
          ),
        ),
        new RaisedButton(
            child: new Text("Voltar"),
            textColor: Colors.white,
            color: Colors.redAccent,
            onPressed: () {
              // Navigate back to first screen when tapped!
              Navigator.pop(context);
            }
        )
      ],
    );
  }
}
