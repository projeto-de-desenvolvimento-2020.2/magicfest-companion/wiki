import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:tcg_companion/pages/artists.dart';
import 'package:tcg_companion/pages/map.dart';
import 'package:tcg_companion/pages/tournaments.dart';
import 'package:tcg_companion/pages/vendors.dart';

import 'drawerWithState.dart';

class EventPage extends StatelessWidget {
  final DocumentSnapshot event;

  const EventPage({Key key, this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(event["name"]),
        backgroundColor: Colors.black,
      ),
      drawer: stateDrawer(),
      body: _body(context),
      backgroundColor: Colors.black,
    );
  }


   _body(context){
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Image.network(
            event["photo"],
            height: 300,
            width: 300,
          ),
          _buttons1(context),
          _buttons2(context),
          _qna()
        ],
      )
    );
  }

  _buttons1(context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new RaisedButton(
            child: new Text("Artistas"),
            textColor: Colors.white,
            color: Colors.redAccent,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Artists()),
              );
            }
        ),
        new RaisedButton(
            child: new Text("Vendedores"),
            textColor: Colors.white,
            color: Colors.redAccent,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Vendors()),
              );
            }
        ),
        new RaisedButton(
            child: new Text("Mapa"),
            textColor: Colors.white,
            color: Colors.redAccent,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Map()),
              );
            }
        ),
    ]
    );
  }
  _buttons2(context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
           RaisedButton(
              child: new Text("Torneios"),
              textColor: Colors.white,
              color: Colors.redAccent,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Tournaments()),
                );
              }
          ),
           RaisedButton(
              child: new Text("Voltar"),
              textColor: Colors.white,
              color: Colors.redAccent,
              onPressed: () {
                // Navigate back to first screen when tapped!
                Navigator.pop(context);
              }
          ),
        ]
    );
  }
  _qna() {
    return Column(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          new Text(
            '\n - O que é um Magic Fest?\n'
            "  ${event['qna1']}\n"
                '\n - Qual horário da Abertura?\n'
                "  ${event['qna']}\n"
                '\n - Qual formato do Evento Principal?\n'
                "  ${event['qna3']}\n"
                '\n - Quanto custa para entrar no evento?\n'
                "  ${event['qna4']}\n"
                '\n - Qual a data de Inicio e Final?\n'
                "  ${event["date"]}\n",
            style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold
            ),
          ),
          Image.network(
            'https://media.magic.wizards.com/Event-Types-MagicFest-Header.jpg',
            height: 300,
            width: 300,
          ),
        ]
    );
  }
}
