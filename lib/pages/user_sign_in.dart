import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../main.dart';
import 'drawerWithState.dart';
import 'lostPassword.dart';

class userSignIn extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  static const String _title = 'Log In de usuário';
  User currentUser;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(_title),
        backgroundColor: Colors.black,
      ),
      drawer: stateDrawer(),
      body: _body(context),
    );
  }

  Container _body(context) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextFormField(
              decoration: const InputDecoration(
                labelText: 'E-mail',
              ),
              controller: emailController,
              keyboardType: TextInputType.text,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Por Favor entre com o E-mail@';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Senha',
              ),
              controller: passwordController,
              keyboardType: TextInputType.text,
              obscureText: true,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Por Favor entre com a sua senha';
                }
                return null;
              },
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: RaisedButton(
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    _logInUser(context);
                  }
                },
                child: Text('Avança'),
              ),
            ),
           RaisedButton(
            onPressed: () {
              Navigator.push(
                context,
              MaterialPageRoute(builder: (context) => lostPassword()),
              );
            },
              child: Text('Esqueci a Senha'),
            )
          ],
        ),
      ),
    );
  }

  Future<void> _logInUser(context) async {
    String email = emailController.text;
    String password = passwordController.text;

    try {
      UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: email,
          password: password,
      );
      currentUser = userCredential.user;
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MyApp()),
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        showAlertDialog1(context, 'E-mail não encontrado');
      } else if (e.code == 'wrong-password') {
        showAlertDialog1(context, 'Senha invalida');
      }
    }
  }

  showAlertDialog1(context, String message) {

    Widget okButton = FlatButton(
      child: Text("Ok"),
      onPressed: () {
            Navigator.of(context).pop();
      },
    );
    // configura o  AlertDialog
    AlertDialog alerta = AlertDialog(
      title: Text(message),
      actions: [
        okButton,
      ],
    );
    // exibe o dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alerta;
      },
    );
  }
}
