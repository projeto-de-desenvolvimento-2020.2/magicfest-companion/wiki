import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:tcg_companion/pages/tournaments.dart';
import 'package:tcg_companion/pages/map.dart';
import 'drawerWithState.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;



class TournamentPage extends StatelessWidget {
  final DocumentSnapshot tournament;
  TournamentPage({Key key, this.tournament}) : super(key: key);
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  static AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
  final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Torneio"),
        backgroundColor: Colors.black,
      ),
      drawer: stateDrawer(),
      body: _body(context),
      backgroundColor: Colors.black,
    );
  }


  _body(context) {
    return Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 10),
            Image.network(
              'https://images.ctfassets.net/ryplwhabvmmk/3xEgfHnM2AvtH9HpGArzuq/4be297e8386d48f3030639fa4c0e4b10/25-World-Championship-XXVI-PVDDR-Winner.jpg',
              height: 320,
              width: 500,
            ),
             Text(
                "${tournament['name']}\n"
                "Hora de inicio: ${tournament['time']}\n"
                r"Valor da Inscrição: R$" "${tournament['subs']}\n"
                "Mesa Inicial: ${tournament['tableTournament']}",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 27,
                  fontStyle: FontStyle.italic,
                    fontFamily: 'Beleren'
                ),
              ),
            SizedBox(height: 10),
            _buttons1(context)
          ],
        )
    );
  }

  _buttons1(context) {
    final url = tournament['urlpay'];
    tz.initializeTimeZones();

    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new RaisedButton(
              child: new Text("Voltar"),
              textColor: Colors.white,
              color: Colors.redAccent,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Tournaments()),
                );
              }
          ),
          new RaisedButton(
              child: new Text("Inscreva-se"),
              textColor: Colors.white,
              color: Colors.redAccent,
              onPressed: () {

                launch(url);

              }
          ),
          new RaisedButton(
              child: new Text("Mapa"),
              textColor: Colors.white,
              color: Colors.redAccent,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Map()),
                );
              }
          ),
        ]
    );
  }
}