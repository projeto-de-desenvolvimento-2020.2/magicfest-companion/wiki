import 'package:flutter/material.dart';

import 'addArtists.dart';
import 'addTournaments.dart';
import 'addVendors.dart';
import 'drawerWithState.dart';
import 'eventupload.dart';

class AdminPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('TCG Companion - Página do Administrador'),
        backgroundColor: Colors.black,
      ),
      drawer: stateDrawer(),
      body: _body(context),
      backgroundColor: Colors.white,
    );
  }

  Column _body(context) {
    return Column(
      children: <Widget>[
            ListTile(
              leading: Icon(Icons.add_box),
              title: Text("Add Event"),
              subtitle: Text("Adicionar Eventos."),
              trailing: Icon(Icons.arrow_forward),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => EventUpload()),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.add_box),
              title: Text("Adiciona Artista"),
              subtitle: Text("Adicionar Artista."),
              trailing: Icon(Icons.arrow_forward),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddArtists()),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.add_box),
              title: Text("Adiciona Torneio"),
              subtitle: Text("Adicionar Torneio."),
              trailing: Icon(Icons.arrow_forward),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddTournaments()),
                );
              },
            ),
          ListTile(
           leading: Icon(Icons.add_box),
            title: Text("Adiciona Vendedores"),
            subtitle: Text("Adicionar Logo e informações do Vendedor."),
            trailing: Icon(Icons.arrow_forward),
            onTap: () {
              Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddVendors()),
            );
          },
        ),
      ],
    );
  }
}
