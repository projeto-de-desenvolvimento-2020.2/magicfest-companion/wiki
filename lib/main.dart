import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:tcg_companion/pages/home.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';


void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  static const String _title = 'TCG Event Companion';
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
  FirebaseAnalyticsObserver(analytics: analytics);
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire:
      future: _initialization,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          print("SomethingWentWrong");
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: _title,
            theme: ThemeData(fontFamily: 'Beleren'),
            home: HomePage(),
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        print("Loading");
        return LoaderWidget();
      },
    );
  }
}
class LoaderWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Column();
  }
}